
/*
 * Auto generated Run-Time-Environment Component Configuration File
 *      *** Do not modify ! ***
 *
 * Project: 'blinky_s110_pca20006' 
 * Target:  'nrf51822_xxaa_s110' 
 */

#ifndef RTE_COMPONENTS_H
#define RTE_COMPONENTS_H

#define BSP_DEFINES_ONLY
#define S110
#define SOFTDEVICE_PRESENT

#endif /* RTE_COMPONENTS_H */
